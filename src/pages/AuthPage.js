import React, {Component} from 'react';
import { Route } from 'react-router-dom';
import { connect } from "react-redux";
import Button from '../components/Button/Button';
import styles from './Auth.module.css';
import { UserSevice } from '../services/user.service';
import { login } from '../redux/appActions';

class Auth extends Component {
    constructor(props) {
        super(props);

        this.state = {
            login: '', 
            password: '',
            isFailed: null
        };

        this.handleInputChange = this.handleInputChange.bind(this);
    }

    async handleClick(history) {
        let user = await UserSevice.getByLoginAndPassword(this.state.login, this.state.password);

        if (this.state.login !== '' && this.state.password !== '' ) {
            if (user) {
                this.props.login(user);
                history.push('/');

            } else {
                this.setState({
                isFailed: true 
                });
            }
        }

        
    }

	handleInputChange = event => {
		this.setState({
			[event.target.name]: event.target.value,
		});
	};

    render() {
        return (
            <div className={styles.form}>
                <h2 className={styles.title}>Авторизация</h2>
                <input name="login" 
                    className={this.state.isFailed ? styles.error : styles.input}
                    value={this.state.login}
                    onChange={this.handleInputChange}>
                </input>
                <input name="password"
                    className={this.state.isFailed ? styles.error : styles.input}
                    value={this.state.password}
                    onChange={this.handleInputChange}>
                </input>
                <Route render={({ history }) => (
                    <Button onClick={this.handleClick.bind(this, history)}>Войти</Button>
                )} />
            </div>
        );
    }
    
}

const mapDispatchToProps = dispatch => ({
    login: payload => dispatch(login(payload))
});
  
export default connect(
    null,
    mapDispatchToProps
)(Auth);
  