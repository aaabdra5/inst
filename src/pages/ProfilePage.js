import React, { Fragment, Component } from 'react';
import { connect } from "react-redux";
import ProfileInfo from '../components/ProfileInfo/ProfileInfo';
import Tabs from '../components/Tabs/Tabs';
import Cards from '../components/Cards/Cards';
import { CardSevice } from '../services/card.service';


class ProfilePage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            cards: [],
            user: props.appState.user
        };
    }
 
    async componentDidMount() {
        this.setState({
            cards: await CardSevice.getByAuthorId(this.state.user.userId)
        });
    }


    render() {
        return (
            <Fragment>
                <ProfileInfo {...this.state.user}></ProfileInfo>
                <Tabs></Tabs>
                <Cards cards={this.state.cards}></Cards>
            </Fragment>
        );
    }
}

const mapStateToProps = appState => ({ appState });

export default connect(
  mapStateToProps,
  null
)(ProfilePage);
