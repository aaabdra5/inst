import React, {Component, Fragment} from 'react';
import Cards from '../components/Cards/Cards';
import Filters from '../components/Filters/Filters';
import { POPULAR_FILTER_TYPE, DISCUSSION_FILTER_TYPE, RANDOM_FILTER_TYPE } from '../components/Filters/filterType';
import { CardSevice } from '../services/card.service';


export default class MainPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            type: POPULAR_FILTER_TYPE,
            cards: []
        };
    }

    async componentDidMount() {
        this.setState({
            cards: await CardSevice.getAllCards()
        });
    }

    async onFilterChanged(type) {
        let cards;

        switch (type) {
            case POPULAR_FILTER_TYPE:
                let allCards = await CardSevice.getAllCards();
                cards = allCards.sort((a, b) => b.likes - a.likes);
                break;

            case DISCUSSION_FILTER_TYPE:
                cards = await CardSevice.getAllCards();
                break;

            case RANDOM_FILTER_TYPE:
                cards = await CardSevice.getAllCards();
                break;
        
            default:
                cards = [];
                break;
        }

        this.setState({
            cards: cards
        });
    }

    render() {
        return (
            <Fragment>
                <Filters onChange={this.onFilterChanged}></Filters>
                <Cards cards={this.state.cards}></Cards>
            </Fragment>
        );
    }
}
