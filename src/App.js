import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";

import './App.css';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import ProfilePage from './pages/ProfilePage';
import MainPage from './pages/MainPage';
import AuthPage from './pages/AuthPage';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      //...users[1]
        // cards: [],
        // user: null,
        // userCards: []
    }
  }

  // async componentDidMount() {
  //   this.setState({
  //     cards: await UserSevice.getAllCards(),
  //     user: await UserSevice.getUser(1),
  //     userCards: await UserSevice.getCardsById(1)
  //   });
  // }

  render() {
    return (
      <Fragment>   
          <Router>
            <Header></Header>
            <Route exact path="/" component={MainPage} />
            <Route path="/profile" component={ProfilePage} />
            <Route path="/auth" component={AuthPage} />
        </Router>
        <Footer></Footer>
      </Fragment>
    );
  }
}

export default App;
