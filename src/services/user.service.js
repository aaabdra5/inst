import users from './mocks/users';
import comments from './mocks/comments';
import { BaseService } from './base.services';

export class UserSevice extends BaseService {

    static getByLoginAndPassword(login, password) {
        let user = users.find(p => p.login === login && p.password === password);
        return super.promiseResponse(user);
    }

    static getUser(id) {
        let user = users.find(p => p.userId === id);
        return super.promiseResponse(user);
    }

    static getComments(userId, imageId) {
        let imageComments = comments.filter(p => p.imageAuthorId === userId && p.imageId === imageId);
        return super.promiseResponse(imageComments);
    }

}