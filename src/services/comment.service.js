import comments from './mocks/comments';
import { BaseService } from './base.services';

export class CommentSevice extends BaseService {

    static getByImageId(id) {
        let commentsArr = comments.filter(p => p.imageId === id);
        return super.promiseResponse(commentsArr);
        
    }



}