import cards from './mocks/cards';
import { BaseService } from './base.services';

export class CardSevice extends BaseService {

    static getAllCards() {
        return super.promiseResponse(cards);
        
    }

    static getByAuthorId(id) {
        let userCards = cards.filter(p => p.authorId === id);
        return super.promiseResponse(userCards);
    }


}