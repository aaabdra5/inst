
export class BaseService {
    static promiseResponse = (data, interval = 200) =>
	    new Promise(resolve => {
		    setTimeout(() => {
			    resolve(data);
		    }, interval);
	});

}