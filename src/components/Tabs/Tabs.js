import React from 'react';
import styles from './Tabs.module.css';
import { TabItem } from '../TabItem/TabItem';
import Button from '../Button/Button';

export default () => {
    return (
        <div className={styles.container}>
            <div className={styles.tabs}>
                <TabItem>Мои публикации</TabItem>
                <TabItem>Сохраненные публикации</TabItem>
                <Button>Добавить публикацию</Button>
            </div>
        </div>
    );
}