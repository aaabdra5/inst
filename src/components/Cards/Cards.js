import React from 'react';
import styles from './Cards.module.css';
import CardItem from '../CardItem/CardItem';


export default ({cards}) => {

    return (
        <div className={styles.container}>
            <div className={styles.cards}>
            {
                cards.map(photo => <CardItem key={photo.id} {...photo}></CardItem>)
            }
            </div>
        </div>

    );
}