import React, {Component} from 'react';
import styles from './TabItem.module.css';

export class TabItem extends Component {
    constructor(props) {
        super(props);

        this.state = {
            checked: false
        }

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.setState({
            checked: true
        })
    }

    render() {
        return (
            <div className={this.state.checked ? styles.checked : styles.tab} onClick={this.handleClick}>
                {this.props.children}
            </div>
        );
    }
}