import React from 'react';
import styles from './Filters.module.css';
import { TabItem } from '../TabItem/TabItem';
import { POPULAR_FILTER_TYPE, DISCUSSION_FILTER_TYPE, RANDOM_FILTER_TYPE } from '../../components/Filters/filterType';

export default () => {
    return (
        <div className={styles.container}>
            <div className={styles.tabs}>
                <TabItem type={POPULAR_FILTER_TYPE}>Популярные</TabItem>
                <TabItem type={DISCUSSION_FILTER_TYPE}>Обсуждаемые</TabItem>
                <TabItem type={RANDOM_FILTER_TYPE}>Случайные</TabItem>
            </div>
        </div>
    );
}