import React, {Component} from 'react';
import Button from '../Button/Button';
import styles from './NewCommentForm.module.css';

export default class NewCommentForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            text: ''
        }
    }

    render() {
        return (
            <form className={styles.form}>
                <textarea className={styles.text}></textarea>
                <Button type="submit">Отправить</Button>
            </form>
        )
    }
}