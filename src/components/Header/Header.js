import React from 'react';
import {Link} from 'react-router-dom';
import { connect } from "react-redux";
import { logout } from '../../redux/appActions';
import styles from './Header.module.css';
import Button from '../Button/Button';


const template = props => (
    <header className={styles.header}>
        <div className={styles.wrapper}>
            <Link to="/">
                <div className={styles.header__logo}>
                    <img src="img/icon-photo.png" width="70px" alt=""></img>
                </div>
            </Link>
            
            { props.appState.user ? (
                    <Link to="/profile">
                        <div className={styles.header__profile}>
                            <div className={styles.name}>{props.appState.user.userName}</div>
                            <img src={props.appState.user.avatar} alt="" width="70px"></img>
                            <Link to="/auth">
                                <Button onClick={props.logout}>logout</Button>
                            </Link>
                        </div>
                    </Link>
            ) : (
                <Link to="/auth">
                    <Button>login</Button>
                </Link>
            )}
            
        </div>
    </header>
);

const mapStateToProps = appState => ({ appState });
const mapDispatchToProps = dispatch => ({
    logout: payload => dispatch(logout(payload))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(template);
