import React, {Component} from 'react';
import styles from './CardItem.module.css';
import OpenCard from '../OpenCard/OpenCard';



class CardItem extends Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false
        }

        this.handleClick = this.handleClick.bind(this);
        this.closePopup = this.closePopup.bind(this);
    }

    handleClick(e) {
        e.preventDefault();
		if (this.state.open === false) {
			this.setState({
				open: true
            });
        } 
    }

    closePopup(e) {
        e.preventDefault();
        this.setState({
            open: false
        });
    }

    render() {

        return (
            <div className={styles.picture} onClick={this.handleClick}>
                <img src={this.props.src} width="100%" alt=""></img>
                {this.state.open ? <OpenCard {...this.props} onClick={this.closePopup}></OpenCard> : null}
            </div>
        )
    }
}

// export default (item) => {

//     return (
//         <div className={styles.picture}>
//             <img src={item.src} width="100%" height="100%" alt=""></img>
//         </div>
//     );
// }

export default CardItem;