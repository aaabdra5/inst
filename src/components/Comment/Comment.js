import React, {Component} from 'react';
import styles from './Comment.module.css';
import { UserSevice } from '../../services/user.service';

// export default ({comment}) => {
//     return(
//         <div className={styles.comment}>
//             <div className={styles.avatar}>
//                 <img src={comment.avatar} alt="" width="70px" height="70px"></img>
//             </div>
            
//             <div>
//                 <div className={styles.name}>{comment.author}</div>
//                 <div className={styles.text}>{comment.text}</div>
//             </div>
//         </div>
//     )
// }

export default class OpenCard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            avatar: '',
            name: ''
        }
    }

    async componentDidMount() {
        let user = await UserSevice.getUser(this.props.commentAuthorId);
        this.setState({
            avatar: user.avatar,
            name: user.userName
        });
      }

    render() {
    return(
        <div className={styles.comment}>
            <div className={styles.avatar}>
                <img src={this.state.avatar} alt="" width="70px" height="70px"></img>
            </div>
            
            <div>
                <div className={styles.name}>{this.state.name}</div>
                <div className={styles.text}>{this.props.text}</div>
            </div>
        </div>
    )
    }
}