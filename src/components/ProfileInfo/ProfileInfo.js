import React, {Component} from 'react';
import styles from './ProfileInfo.module.css';
import Button from '../Button/Button';

export default class ProfileInfo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isEdit: false,
            name: this.props.userName,
            about: this.props.about
        }

        this.onEdit = this.onEdit.bind(this);
        // this.onSave = this.onSave.bind(this);
        // this.onInputChange = this.onInputChange.bind(this);

        this.name = React.createRef();
        this.about = React.createRef();
    }

    onEdit(e) {
        e.preventDefault();
        this.setState({
            isEdit: true
        })
    }

    // onSave(e) {
    //     e.preventDefault();
        
    //     this.setState({
    //         isEdit: false,
    //         name: this.name.value,
    //         about: this.about.value
    //     })
    // }

    // onInputChange(e) {
    //     this.setState({
    //         [e.target.name]: e.target.value
    //     })
    // }

    render() {
        return (
            !this.state.isEdit ?
            <div className={styles.container}>
                <div className={styles.info}>
                    <div className={styles.photo}>
                        <img src={this.props.avatar} alt="" width="100%"></img>
                    </div>
                    <div className={styles.text}>
                        <div className={styles.name}>{this.props.userName}</div>
                        <div className={styles.about}>{this.props.about}</div>
                        <Button onClick={this.onEdit}>Редактировать профиль</Button>
                    </div> 
                </div> 
            </div> 
            : 
            <div className={styles.container}>
                <div className={styles.info}>
                    <div className={styles.photo}>
                        <img src={this.props.avatar} alt="" width="100%" height="100%"></img>
                    </div>
                    <div className={styles.text}>
                        <input name="name" className={styles.name} ref={this.name} onChange={this.onInputChange} value={this.state.name}></input>
                        <textarea name="about" className={styles.about} ref={this.about} onChange={this.onInputChange} value={this.state.about}></textarea>
                        <Button onClick={this.onSave}>Сохранить</Button>
                    </div> 
                </div>
            </div>    
        )
    }
}

 


