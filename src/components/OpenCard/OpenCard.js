import React, {Component} from 'react';
import styles from './OpenCard.module.css';
import Comment from '../Comment/Comment';
import NewCommentForm from '../NewCommentForm/NewCommentForm';
import { UserSevice } from '../../services/user.service';
import { CommentSevice } from '../../services/comment.service';


export default class OpenCard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            cardAuthor: '',
            commentAuthor: '',
            comments: [],
            likes: this.props.likes,
            addLike: false
        }

        this.addLike = this.addLike.bind(this);
    }

    async componentDidMount() {
        this.setState({
          author: await UserSevice.getUser(this.props.authorId),
          comments: await CommentSevice.getByImageId(this.props.id)
        });
      }

    addLike() {
        !this.state.addLike ?
        this.setState({
            likes: this.state.likes + 1,
            addLike: true
        }):
        this.setState({
            likes: this.state.likes - 1,
            addLike: false
        })
    }

    render() {
        return (
            <div className={styles.overlay}>
                <span className={styles.close} onClick={this.props.onClick}>&times;</span>
                <div className={styles.preview}>
                    <img src={this.props.src} className={styles.image} alt="" width="800px"></img>
                    <div className={styles.controls}>
                        <div className={styles.like}>Нравится &nbsp;
                            <span className={!this.state.addLike ? styles.likes__count : styles.likes__count__liked} 
                                onClick={this.addLike}>
                                    {this.state.likes}
                            </span>
                        </div>
                        <div className={styles.comments}>
                            <span className="comments-count">
                                {this.state.comments.length}
                            </span> комментариев
                        </div>
                    </div>
                    {this.state.comments.map((comment) => <Comment key={comment.commentId} {...comment}></Comment>)}
    
                    <NewCommentForm></NewCommentForm>
                </div>
            </div>
        );
    }
}