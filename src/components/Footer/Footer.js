import React from 'react';

import styles from './Footer.module.css'

export default () => 
<footer className={styles.footer}>
  <p className="footer__text">© 2019 Тинькофф Финтех</p>
</footer>